"""Visualization tools for noise generators"""
import numpy as np
from dataclasses import dataclass
import plotly.graph_objects as go
from ..noise.abc import AbstractNoise


def _range(A):
    return (np.nanmin(A), np.nanmax(A))


@dataclass
class NoisePlotFactory(AbstractNoise):
    _noisegen: AbstractNoise

    def _generate(self, *slices):
        noise = self._noisegen._generate(*slices)

        x, y, z, *_ = [d.reshape(-1) for d in np.ogrid[slices]] + [None, None]
        if len(slices) == 1:
            return NoisePlot1D(noise, x)
        elif len(slices) == 2:
            return NoisePlot2D(noise, x, y)
        elif len(slices) == 3:
            return NoisePlot3D(noise, x, y, z)


@dataclass
class NoisePlot1D:
    _noise: np.ndarray
    _x: np.ndarray

    def scatterline(self, y_range=None):
        """Plot the resulting noise as a scatterplot with linear interpolation

        Parameters
        ----------
        y_range : [y1, y2], optional
            The range of the y-axis, [-1, 1] by default

        Returns
        -------
        figure : :class:`plotly.go`
        """
        y_range = y_range or [-1, 1]

        figure = go.Figure(
            data=go.Scatter(
                x=self._x,
                y=self._noise,
            )
        )
        figure.update_yaxes(range=y_range)
        return figure


@dataclass
class NoisePlot2D:
    _noise: np.ndarray
    _x: np.ndarray
    _y: np.ndarray

    def surface(self, z_scale=None, z_range=None, contour=True):
        z_range = z_range or [-1, 1]
        z1, z2 = z_range

        x_dist = np.ptp(self._x)
        y_dist = np.ptp(self._y)
        z_dist = z2 - z1

        z_scale = z_scale or min(x_dist, y_dist) / z_dist

        figure = go.Figure(
            data=go.Surface(
                x=self._x,
                y=self._y,
                z=self._noise,
                contours=dict(
                    z=dict(
                        show=True,
                        start=z1,
                        end=z2,
                        size=z_dist / 20
                    )
                ) if contour else None
            )
        )

        # Manually determine scaling
        # We want to maintain scale between the x- and y- axes, but
        # these typically have large range (>1) whereas the z-axis
        # typically is normalized to [0, 1]. Therefore we scale the
        # z-axis manually and separately.
        scale = min(x_dist, y_dist, z_scale)
        figure.update_layout(
            scene=dict(
                aspectmode='manual',
                aspectratio=dict(
                    x=x_dist / scale,
                    y=y_dist / scale,
                    z=z_scale / scale
                ),
                zaxis=dict(nticks=5, range=z_range)
            )
        )
        return figure

    def voxelspace(self, z_scale=None, z_range=None, z_resolution=32, size=1):
        z_range = z_range or [-1, 1]
        z1, z2 = z_range

        x_dist = np.ptp(self._x)
        y_dist = np.ptp(self._y)
        z_dist = z2 - z1

        z_scale = z_scale or min(x_dist, y_dist) / z_dist

        z, y, x = np.where(np.array([self._noise > z for z in np.linspace(z1, z2, z_resolution)]))
        z = z / z_resolution

        figure = go.Figure(data=go.Scatter3d(
            x=x, y=y, z=z,
            mode='markers',
            marker=dict(
                size=size,
                color=z,
                colorscale='Viridis'
            )
        ))

        scale = min(x_dist, y_dist, z_scale)
        figure.update_layout(
            scene=dict(
                aspectmode='manual',
                aspectratio=dict(
                    x=x_dist / scale,
                    y=y_dist / scale,
                    z=z_scale / scale
                ),
                xaxis=dict(range=_range(self._x)),
                yaxis=dict(range=_range(self._y)),
                zaxis=dict(range=z_range)
            )
        )
        return figure

    def spheremap(self, r_range=None):
        r_range = r_range or [0.5, 1]
        r1, r2 = r_range

        r = (self._noise / 2 + 0.5) * (r2 - r1) + r1
        nx, ny = r.shape
        phi, theta = np.meshgrid(np.linspace(0, np.pi, ny), np.linspace(0, 2 * np.pi, nx))
        figure = go.Figure(
            data=go.Surface(
                x=r * np.sin(phi) * np.cos(theta),
                y=r * np.sin(phi) * np.sin(theta),
                z=r * np.cos(phi),
                surfacecolor=r
            )
        )
        return figure

    def barrelmap(self, r_range=None):
        r_range = r_range or [0.5, 1]
        r1, r2 = r_range

        r = (self._noise / 2 + 0.5) * (r2 - r1) + r1
        nx, ny = r.shape
        theta = np.linspace(0, 2 * np.pi, ny)
        z = np.tile(np.linspace(-1, 1, nx), (ny, 1)).T
        figure = go.Figure(
            data=go.Surface(
                x=r * np.cos(theta),
                y=r * np.sin(theta),
                z=z,
                surfacecolor=r
            )
        )
        return figure

@dataclass
class NoisePlot3D:
    _noise: np.ndarray
    _x: np.ndarray
    _y: np.ndarray
    _z: np.ndarray

    def voxelspace(self, threshold=0.5, invert=False, size=1):
        if invert:
            z, y, x = np.where(self._noise >= threshold)
        else:
            z, y, x = np.where(self._noise < threshold)

        figure = go.Figure(data=go.Scatter3d(
            x=x, y=y, z=z,
            mode='markers',
            marker=dict(
                size=size,
                color=z,
                colorscale='Viridis'
            )
        ))

        figure.update_layout(
            scene=dict(
                aspectmode='data',
                xaxis=dict(range=_range(self._x)),
                yaxis=dict(range=_range(self._y)),
                zaxis=dict(range=_range(self._z))
            )
        )
        return figure

    def volumetric(self, threshold=0.4):
        X = np.rot90(np.tile(self._x, (len(self._z), len(self._y), 1)), axes=(2, 0))
        Y = np.rot90(np.tile(self._y, (len(self._x), len(self._z), 1)), axes=(2, 1))
        Z = np.tile(self._z, (len(self._x), len(self._y), 1))
        figure = go.Figure(data=go.Volume(
            x=X.flatten(), y=Y.flatten(), z=Z.flatten(),
            value=self._noise.flatten(),
            isomin=threshold,
            isomax=1.0,
            opacity=0.1,
            surface_count=25
        ))
        return figure
