"""Spectral synthesis noise generation"""
import numpy as np

_DEFAULT_SEED = 0
_PCG32_MULT = 0x5851f42d4c957f2d


def _pcg32(points):
    """Quick-n-dirty solid-state PCG32 PRNG implementation"""
    points = points.astype(np.uint)
    seed = points[:, 0]
    inc = points[:, 1] << 1 | 1
    state = (np.zeros_like(seed) + inc + seed) * _PCG32_MULT + inc

    xorshifted = ((state >> 18) ^ state) >> 27
    rot = state >> 59
    return (xorshifted >> rot) | (xorshifted << ((-rot + 1) & 31))


def _lerp_2d(x, y, lattice):
    """Vectorized linear interpolation in two dimensions"""
    my = lattice[1, :] - lattice[0, :]
    ly = lattice[0, :] + my * y[np.newaxis].T
    mx = ly[:, 1] - ly[:, 0]
    return ly[:, 0] + mx * x[np.newaxis].T


def _noise(amplitude, lattice_pts, dim, seed=_DEFAULT_SEED):
    tx = np.linspace(0, 1, dim[0])
    tz = np.linspace(0, 1, dim[1])

    # Compute lattice
    lattice = _pcg32(
        seed + lattice_pts.reshape(4, 2)
    ).reshape(2, 2) % amplitude

    # Smoothstep function
    sx = 3*tx**2 - 2*tx**3
    sz = 3*tz**2 - 2*tz**3

    # Lerp it
    return _lerp_2d(sx, sz, lattice)


def _layered_noise(dim, offset=None, n_layers=3, max_amplitude=20, persist=0.6, seed=_DEFAULT_SEED):
    dim = np.array(dim, dtype=np.uint)
    if offset is None:
        offset = (0, 0)
    offset = np.array(offset, dtype=np.uint)
    noise = np.zeros(dim)
    total_amplitude = 0
    for i in range(1, n_layers+1):
        amplitude = (persist ** (n_layers-i)) * max_amplitude
        a = offset % i
        b = a+1
        a *= dim
        b *= dim
        lattice_pts = i * (offset // i) + \
            np.array([[[0, 0], [1, 0]], [[0, 1], [1, 1]]], dtype=np.uint) * i
        layer = _noise(
            amplitude,
            lattice_pts=lattice_pts,
            dim=dim * i,
            seed=seed
        )
        noise += layer[a[0]:b[0], a[1]:b[1]]
        total_amplitude += amplitude
    return noise / total_amplitude


class SpectralSynth2D:
    def __init__(self, resolution=(8, 8), n_layers=3, max_amplitude=64, persist=0.6, seed=_DEFAULT_SEED):
        self._resolution = np.array(resolution, dtype=np.uint)
        self._n_layers = n_layers
        self._max_amplitude = max_amplitude
        self._persist = persist
        self._seed = seed

    def generate(self, width, length, x=0, y=0):
        return np.block(
            [
                [
                    _layered_noise(
                        self._resolution,
                        offset=(i+x, j+y),
                        n_layers=self._n_layers,
                        max_amplitude=self._max_amplitude,
                        persist=self._persist,
                        seed=self._seed
                    )
                    for j in range(length)
                ] for i in range(width)
            ]
        )
