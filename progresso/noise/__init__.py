"""Multidimensional noise generators"""
# flake8: noqa F401

from .abc import AbstractNoise
from .perlin import PerlinNoise
