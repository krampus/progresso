"""ABC and utilities for noise generators"""
from abc import abstractmethod
from numbers import Number
from typing import Callable, Tuple
from dataclasses import dataclass
import numpy as np
from functools import wraps


def _cast_other(fn):
    """Helper decorator to cast the `other` parameter to a noise generator when possible"""
    @wraps(fn)
    def decorator(self, other):
        if not isinstance(other, AbstractNoise):
            other = ConstantNoise(other)
        return fn(self, other)
    return decorator


class AbstractNoise:
    """ABC for multidimensional noise generators"""

    @abstractmethod
    def _generate(self, *slices):
        pass

    def translate(self, offset):
        pass  # TODO

    def __getitem__(self, key):
        if not isinstance(key, tuple):
            key = (key,)

        slices = tuple(dim if isinstance(dim, slice) else slice(dim, dim+1, 1) for dim in key)
        return self._generate(*slices)

    def __neg__(self):
        return ComplementNoise(self)

    @_cast_other
    def __or__(self, other):
        return UnionNoise(self, other)

    __ror__ = __or__

    @_cast_other
    def __and__(self, other):
        return IntersectionNoise(self, other)

    __rand__ = __and__

    @_cast_other
    def __sub__(self, other):
        # Set difference := intersection(complement(other))
        return IntersectionNoise(self, ComplementNoise(other))

    @_cast_other
    def __rsub__(self, other):
        return IntersectionNoise(other, ComplementNoise(self))

    def __floordiv__(self, other):
        return TransformNoise(self, other)


@dataclass
class ConstantNoise(AbstractNoise):
    """A noise generator that yields a constant value across the domain"""
    _value: Number

    def _generate(self, *slices):
        shape = tuple(d.stop - d.start for d in slices)
        return np.full(shape, self._value)


@dataclass
class ComplementNoise(AbstractNoise):
    """The inverse of a noise generator on the domain [0, 1]"""
    _base: AbstractNoise

    def _generate(self, *slices):
        return 1 - self._base._generate(*slices)


@dataclass
class UnionNoise(AbstractNoise):
    """A noise generator that yields the maximum of its two operand generators"""
    _a: AbstractNoise
    _b: AbstractNoise

    def _generate(self, *slices):
        return np.amax((self._a._generate(*slices), self._b._generate(*slices)), axis=0)


@dataclass
class IntersectionNoise(AbstractNoise):
    """A noise generator that yields the minimum of its two operand generators"""
    _a: AbstractNoise
    _b: AbstractNoise

    def _generate(self, *slices):
        return np.amin((self._a._generate(*slices), self._b._generate(*slices)), axis=0)


@dataclass
class TransformNoise(AbstractNoise):
    """A noise wrapper that transforms the underlying generator with a transformation function"""
    _base: AbstractNoise
    _tx: Callable[[np.ndarray], np.ndarray]

    def _generate(self, *slices):
        return self._tx(self._base._generate(*slices))


@dataclass
class TranslatedNoise(AbstractNoise):
    """A noise wrapper that translates the underlying generator by a constant offset"""
    _base: AbstractNoise
    _offset: Tuple[float, float, float]

    def _generate(self, *slices):
        slices = tuple(slice(s.start + d, s.stop + d, s.step) for s, d in zip(slices, self._offset))
        return self._base._generate(*slices)


def bias(base, bias):
    return TransformNoise(base, lambda x: x + bias)


def polynomial(base, coefficients):
    return TransformNoise(base, lambda x: np.polynomial.polynomial.polyval(x, coefficients))


def cubic(base, a=4, b=-6, c=3, d=0):
    return TransformNoise(base, lambda x: a * x**3 + b * x**2 + c * x + d)


def quadratic(base, a=2, b=-2, c=1):
    return TransformNoise(base, lambda x: a * x**2 + b * x + c)
