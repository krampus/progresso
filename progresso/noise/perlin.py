"""Perlin noise"""
import numpy as np
import noise

from .abc import AbstractNoise


class _PerlinNoiseBase(AbstractNoise):
    """ABC for Perlin noise generators"""
    def __init__(self, scale=256, octaves=1, persistence=0.5, lacunarity=2.0, repeatx=1024, repeaty=1024, repeatz=1024):
        self._scale = scale
        params = dict(
            octaves=octaves,
            persistence=persistence,
            lacunarity=lacunarity
        )
        self._params1 = dict(
            **params,
            repeat=repeatx
        )
        self._params2 = dict(
            **params,
            repeatx=repeatx,
            repeaty=repeaty
        )
        self._params3 = dict(
            **self._params2,
            repeatz=repeatz
        )

    def _noise1(self, x):
        return np.array([self._noisefn1(xi, **self._params1) for xi in x.reshape(-1) / self._scale])

    def _noise2(self, x, y):
        return np.array([
            [
                self._noisefn2(xi, yi, **self._params2)
                for xi in x.reshape(-1) / self._scale
            ] for yi in y.reshape(-1) / self._scale
        ])

    def _noise3(self, x, y, z):
        return np.array([
            [
                [
                    self._noisefn3(xi, yi, zi, **self._params3)
                    for xi in x.reshape(-1) / self._scale
                ] for yi in y.reshape(-1) / self._scale
            ] for zi in z.reshape(-1) / self._scale
        ])

    def _generate(self, *slices):
        x, y, z, *_ = np.ogrid[slices] + [None, None]

        if z is not None:
            # 3D
            ret = self._noise3(x, y, z)
        elif y is not None:
            # 2D
            ret = self._noise2(x, y)
        else:
            # 1D
            ret = self._noise1(x)

        # # Normalize noise to [0, 1]
        # return ret / 2 + 0.5
        return ret


class PerlinNoise(_PerlinNoiseBase):
    _noisefn1 = noise.pnoise1
    _noisefn2 = noise.pnoise2
    _noisefn3 = noise.pnoise3


class SimplexNoise(_PerlinNoiseBase):
    _noisefn1 = noise.pnoise1
    _noisefn2 = noise.snoise2
    _noisefn3 = noise.snoise3
