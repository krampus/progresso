"""A simple toolkit for procedural generation, modelling, and noise"""

from setuptools import setup, find_packages

extras = {
    'tools': [
        'pandas',
        'plotly',
    ],
    'testing': [
        'coverage',
        'nose',
        'nose-timer',
        'rednose',
    ],
    'develop': [
        'autopep8',
        'bpython',
        'flake8',
        'jedi',
        'snakeviz',
        'yapf',
    ]
}

extras['complete'] = list({pkg for req in extras.values() for pkg in req})

setup(
    name='progresso',
    use_scm_version=True,
    author='Rob',
    author_email='contact@robkel.ly',
    description=__doc__,
    url="TODO",
    packages=find_packages(),
    classifiers=[
        "Development Status :: 1 - Planning",
        "License :: OSI Approved :: MIT License"
    ],
    python_requires='>=3.7',
    setup_requires=[
        'setuptools_scm'
    ],
    install_requires=[
        'noise',
        'numpy',
        'plotly',
    ],
    extras_require=extras
)
